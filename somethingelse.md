# Readme for EKS or DigitalOcean implementation of the Kubenetes Cluster: 
###
###
###
# The Basics:
√  EKS is the Elastic Kubernetes Service, an offering by AWS. To use EKS, we have validated the usage of Cloudformation Templates to spin up the EKS cluster, the nodes and deploying the servics. The approach ended up becoming cumbersome in terms of implementation and maintanence. 

√  We thus turned to EKSCTL, which is offered by WeaveWorks - https://eksctl.io/
###
###
###
# How to get started with EKSCTL?
Here are a bunch of commands, that will create an EKS Cluster for you in AWS: 

On a Mac, run using brew:
``` sh 
$ brew tap weaveworks/tap

$ brew install weaveworks/tap/eksctl
 
$ eksctl create cluster --name <NameOfYourCluster> --version <1.10/1.11/1.12(latest)> --nodegroup-name <NameOfYourNodeGroupName> --node-type <EC2InstanceType> --nodes <DesiredCapacityOfNodes> --nodes-min <MinCapacityOfNodes> --nodes-max <MaxCapacityOfNodes> --node-ami <ami-0c57db5b204001099>

```

The AMI can be found here - https://docs.aws.amazon.com/eks/latest/userguide/eks-optimized-ami.html 

###
###
###
#### [To Create the Cluster using the template:](To Create the Cluster using the template:)
``` sh 
$ eksctl create cluster -f infrastructure/eksgeneric/eksstage.template

```
###
###
###
#### [HELM Init:](HELM Init:)
``` sh 
$ kubectl create -f infrastructure/eksgeneric/helm/rbac.yaml

$ helm init --service-account tiller

```
###
###
###
#### [Installing Cert Manager:](Installing Cert Manager:)
``` sh 
$ kubectl get namespaces

$ kubectl get pods --all-namespaces

$ kubectl create namespace cert-manager

$ kubectl label namespace cert-manager certmanager.k8s.io/disable-validation=true

$ kubectl apply -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.6/deploy/manifests/cert-manager.yaml --validate=false

$ helm install --name cert-manager --namespace cert-manager stable/cert-manager

```
###
###
###
#### [Installing LetsEncrypt Certificates & External DNS:](nstalling LetsEncrypt Certificates & External DNS:)
``` sh
$ kubectl create namespace <namespace_name_to_host_services>

$ kubectl apply -f infrastructure/cert-manager/eks/certificate-api.yaml

$ kubectl apply -f infrastructure/cert-manager/eks/issuer-letsencrypt-dns.yml

$ kubectl apply -f infrastructure/cert-manager/eks/certificate-api.yaml

$ kubectl apply -f infrastructure/external-dns/eks/external-dns.yml -n <namespace_name_to_host_services>

```
###
###
###
#### [Installing Prometheus, Grafana and Alertmanager:](Installing Prometheus, Grafana and Alertmanager:)
``` sh
# create a file called alertmanager.yaml with the following data: 

# Not required everytime. 
$ cat <<EoF > alertmanager.yaml 
global:
  resolve_timeout: 5m
route:
  group_by: 
  - job
  group_interval: 5m
  group_wait: 30s
  receiver: pager-duty
  repeat_interval: 12h
  routes: 
  - match: 
      alertname: Watchdog
    receiver: pager-duty  
receivers:
- name: 'pager-duty'
  pagerduty_configs:
    - service_key: 7c76657422c84d2f871b2454d944e107
EoF

# You can create the secret, by running the command: 
$ kubectl create secret generic alertmanager-prometheus --from-file=<path_to_alertmanager_yaml>/alertmanager.yaml

# Not to be implemented every run
$ kubectl get secret alertmanager-prometheus -o json | jq -r '.data["alertmanager.yaml"]' 

# Pass the above value into the file - infrasctructure/kube-prometheus/mainfests/alertmanager-secret.yaml at Line # 3
# Then run :
$ kubectl create -f infrasctructure/kube-prometheus/mainfests/

```
##### Note: 
      Applying the Prometheus bundle at this step, because Nginx has a dependency of Service Monitors, 
      that are a part of the Custom Resource Deployments of the Prometheus Operator Bundle. 
###
###
###
#### [Installing Nginx :](Installing Nginx:)
``` sh 
$ helm install --namespace <namespace_name_to_host_services> --name nginx-ingress stable/nginx-ingress -f infrastructure/nginx-ingress/eks/nginx-external.yaml

$ kubectl apply -f infrastructure/nginx-ingress/ingress-rules/eks.yaml

$ kubectl get certificates -n cert-manager

```
###
###
###
#### [Redis Installation and Validation:](Redis Installation and Validation:)
``` sh 
$ kubectl create namespace monitoring

$ helm install stable/redis --name redis --values infrastructure/redis/eks/redis.yml --namespace monitoring

$ kubectl port-forward --namespace monitoring svc/redis-master 6379:6379

#On your Mac,
$ brew tap ringohub/redis-cli

$ brew update && brew doctor

$ brew install redis-cli

$ redis-cli -h 127.0.0.1 -p 6379
# You are now inside the redis CLI. 
# Enter - "INFO" to validate if the Redis data store has been installed.

```
###
###
###
#### [OPTIONAL If you have not set the withAddOn Policy when creating the EKS Cluster, from the template using EKSCTL:](OPTIONAL If you have not set the withAddOn Policy when creating the EKS Cluster, from the template using EKSCTL:)
``` sh 
$ INSTANCE_PROFILE_NAME=$(aws iam list-instance-profiles | jq -r '.InstanceProfiles[].InstanceProfileName' | grep nodegroup | grep <namespace_name_to_host_services>)

$ INSTANCE_PROFILE_ARN=$(aws iam get-instance-profile --instance-profile-name $INSTANCE_PROFILE_NAME | jq -r '.InstanceProfile.Arn')

$ ROLE_NAME=$(aws iam get-instance-profile --instance-profile-name $INSTANCE_PROFILE_NAME | jq -r '.InstanceProfile.Roles[] | .RoleName')

$ echo "export ROLE_NAME=${ROLE_NAME}" >> ~/.bash_profile

$ echo "export INSTANCE_PROFILE_ARN=${INSTANCE_PROFILE_ARN}" >> ~/.bash_profile

#Create a IAM Policy file such as:
$ mkdir ~/environment/asg_policy

$ cat <<EoF > ~/environment/asg_policy/k8s-asg-policy.json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "autoscaling:DescribeAutoScalingGroups",
        "autoscaling:DescribeAutoScalingInstances",
        "autoscaling:SetDesiredCapacity",
        "autoscaling:TerminateInstanceInAutoScalingGroup"
      ],
      "Resource": "*"
    }
  ]
}
EoF

$ aws iam put-role-policy --role-name $ROLE_NAME --policy-name ASG-Policy-For-Worker --policy-document file://~/environment/asg_policy/k8s-asg-policy.json


```
###
###
###
#### [Installing the Cluster Autoscaler:](Installing the Cluster Autoscaler:)

``` sh
# Cluster autoscaler Yaml, should contain the tags of the AutoScaling Group, in the definition.
# The Cluster AutoScaler, works on the Auto Discovery mode. When there are too many pods and there is not enough nodes, the Cluster AutoScaler, spawns an additional instance to handle the pods. 
# The Cluster Autoscaler, works with EKS in AWS. DigitalOcean, does not currently support Autoscaler and only manual updates to the cluster are supported as of May 2019

$ kubectl apply -f infrastructure/cluster-autoscaler/eks/cluster_autoscaler.yml

$ kubectl logs -f deployment/cluster-autoscaler -n kube-system


```
###
###
###
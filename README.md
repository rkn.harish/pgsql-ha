###########
pgsql-ha
###########
Steps I have used:
1. I have used an Ubuntu AMI for this exercise. 
2. Installing Kubernetes and KOPS
3. The CFT vpc.yml will create the following resources: 
   a. VPC
   b. Public Subnet
   c. Private Subnet
   d. IGW, NAT Gateway, Routing tables
   e. Security Groups
   f. S3 bucket for kops to persist its state. 

4. I was having issues with ssh-keygen. If this does not run as per the CFT, then sometimes, I had to manually login to the box and start from this step. 

5. The following commands can be used for a dashboard of the Kube cluster and health 
  kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/master/src/deploy/recommended/kubernetes-dashboard.yaml
  kops get secrets kube --type secret -oplaintext

6. The files in the Tiller directory will be used for setting rbac and role binding. 

7. For installing Helm and Tiller I have created a cluster with RBAC enabled. Install one Tiller for the PGSQL entity with the --service-account flag. I have used a Role and RoleBindings.

8. For the Cluster autoscaler, I have used kubectl to apply the addon "cluster-autoscaler". The file that is used for this configuration is in the Helm Directory. 

9. To install PostgresSQL, I have used the default file provided in the helm charts repo. I have only tweaked the number of replicas that will be provisioned. 

10. All of the commands are as part of the User Data section on the HostServer.yml under the CloudFormation directory.  

11. Ensure that the IAM role has the following policy before the autoscaler is invoked. 
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "autoscaling:DescribeAutoScalingGroups",
                "autoscaling:DescribeAutoScalingInstances",
                "autoscaling:DescribeLaunchConfigurations",
                "autoscaling:DescribeTags",
                "autoscaling:SetDesiredCapacity",
                "autoscaling:TerminateInstanceInAutoScalingGroup"
            ],
            "Resource": "*"
        }
    ]
} 

12. After the service account is created via the CFT, I have performed the following steps: 
  a. Copied over the files from Tiller/ directory into the Host instance. and then 
  export NAMESPACE="devqa"
  export NAME_OF_PGSQL="pgsqldevqa"
  export TILLER_SERVICE_ACCOUNT_NAME="tiller-devqa"
  kubectl apply -f tiller-rbac.yml
  kubectl apply -f binding.yml
  
  b. Performed a Helm init so that tiller will now be installed on the namespace such as "devqa"
  helm init --tiller-namespace $NAMESPACE --service-account $TILLER_SERVICE_ACCOUNT_NAME

  kubectl --namespace $NAMESPACE get pods | grep tiller

  c. Using the postgresql.yml file, in the Helm directory, I have installed pgsql: 
  helm install --namespace $NAMESPACE --tiller-namespace $NAMESPACE --name $NAME_OF_PGSQL stable/postgresql -f postgresql.yml

  d. kubectl get pods --namespace $NAMESPACE
     kubectl describe pods <pod-name> --namespace $NAMESPACE
  
  e. Now to have autoscaler deployed on the cluster:
     kubectl apply -f aws-values.yml

13. Some steps I did to validate:
    kubectl get pods --namespace $NAMESPACE -o wide | grep -v NAME
    kubectl cordon <Name_of_Instance_hosting_pgsql>
    kubectl get pods --namespace $NAMESPACE -o wide | grep -v NAME
    kubectl get nodes
    kubectl get pods --namespace $NAMESPACE
    kubectl delete pod <pgsql_pod_name> --namespace $NAMESPACE
    kubectl get pods --namespace $NAMESPACE
    kubectl uncordon <Name_of_Instance_hosting_pgsql>
    kubectl get nodes
    kubectl get pods --namespace $NAMESPACE
    
###########


Additional Commands I have used:

#To Login to master:

ssh -i ~/.ssh/id_rsa admin@api.pgsql.alt.k8s.local
kops validate cluster --state $KOPS_STATE_STORE
kubectl get nodes --show-labels

#To get the password for "postgres" run:
export POSTGRESQL_PASSWORD=$(kubectl get secret --namespace <NameSpace> <Pod_Name> -o jsonpath="{.data.postgresql-password}" | base64 --decode)

#To connect to your database run the following command:
kubectl run <Pod_Name>-client --rm --tty -i --restart='Never' --namespace <NameSpace> --image bitnami/postgresql --env="PGPASSWORD=$POSTGRESQL_PASSWORD" --command -- psql --host <Pod_NAME> -U postgres

#To connect to your database from outside the cluster execute the following commands:
kubectl port-forward --namespace <NameSpace> svc/<Pod_Name> 5432:5432 & psql --host 127.0.0.1 -U postgres

#Check if tiller is running on the devqa namespace
kubectl --namespace <NameSpace> get pods | grep tiller

#Delete the helm releases
helm ls --all
helm del --purge <Name> --tiller-namespace <Namespace>
kubectl get services  




# The Basics:
√  EKS is the Elastic Kubernetes Service, an offering by AWS. To use EKS, we have validated the usage of Cloudformation Templates to spin up the EKS cluster, the nodes and deploying the servics. The approach ended up becoming cumbersome in terms of implementation and maintanence. 

√  We thus turned to EKSCTL, which is offered by WeaveWorks - https://eksctl.io/

# How to get started with EKSCTL?
Here are a bunch of commands, that will create an EKS Cluster for you in AWS: 

On a Mac, run using brew:
``` sh 
$ brew tap weaveworks/tap
$ brew install weaveworks/tap/eksctl

```
``` sh 
$ eksctl create cluster --name <NameOfYourCluster> --version <1.10/1.11/1.12(latest)> --nodegroup-name <NameOfYourNodeGroupName> --node-type <EC2InstanceType> --nodes <DesiredCapacityOfNodes> --nodes-min <MinCapacityOfNodes> --nodes-max <MaxCapacityOfNodes> --node-ami <ami-0c57db5b204001099>
```
The AMI can be found here - 

#### [Installing the Cluster Autoscaler :](Installing the Cluster Autoscaler :)


1. How long did you spend on the coding? What would you chage if you had more time?
	I have worked with Kubernetes before and it was only a POC that I worked on, as a team. I have not experimented with KOPTs or any Production level setup for Kubernetes. 
	This exercise made me read about, in depth about Helm and Tiller, before implementing it.
	I spent about 10 hours in all, trying to get the setup on my AWS account. 
	I kept running in to issues with Helm setup and installing tiller in a separate namespace. I initially used an EC2 instance with RHEL 7. I had to then switch to an AMI of Ubuntu that resolved some package management issues. 
	I then ran into issues with auto-scaler. Where I first used Helm cluster-autoscaler and ran into issues because of how the ./cluster-autoscaler script was not setup properly and the git page had poor documentation. 
	I then had to research why this would not work for me and then started to use an "aws-values.yml" file and ran kubectl apply as an add-on, to enable autoscaling. 

2. Did you learn anything new?

Absolutely! I learnt how tiller can be deployed on a separate name space. I learnt how RBAC can be enabled. 
I now have a better understanding of how this setup works. 

3. What would you have done if you had more time?
I would have worked better on automating this setup, got it into a CI/CD pipeline. Polished out the pieces, basically. 
I realize there are pieces of it that need to be manually run. If I could dedicate more time to this, I am sure I can definitely get this working in a PRODUCTION level environment. 